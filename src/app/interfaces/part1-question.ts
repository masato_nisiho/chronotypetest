export interface Part1Question {
  id: number;
  text: string;
  answer: boolean;
}
