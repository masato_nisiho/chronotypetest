export interface Part2Question {
  id: number;
  text: string;
  choices: [
    { choice_text: string; checked: boolean },
    { choice_text: string; checked: boolean },
    { choice_text: string; checked: boolean }
  ];
}
