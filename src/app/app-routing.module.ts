import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TestPart1Component } from "./pages/test-part1/test-part1.component";
import { TestPart2Component } from "./pages/test-part2/test-part2.component";
import { ResultPart1Component } from "./pages/result-part1/result-part1.component";
import { ResultPart2Component } from "./pages/result-part2/result-part2.component";
import { ListOfTypeComponent } from "./pages/list-of-type/list-of-type.component";

const routes: Routes = [
  { path: "", redirectTo: "/", pathMatch: "full" },
  { path: "test-part1", component: TestPart1Component },
  { path: "test-part2", component: TestPart2Component },
  { path: "result-part1", component: ResultPart1Component },
  { path: "result-part2", component: ResultPart2Component },
  { path: "list-of-type", component: ListOfTypeComponent },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
