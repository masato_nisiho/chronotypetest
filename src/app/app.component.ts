import { Component, OnInit } from "@angular/core";

// Authを利用したく追加したもの
import { AuthService } from "./service/auth/auth.service";
// PWA及び通知機能を利用したく追加したもの
import { MessagingService } from "./service/messaging.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  // Authを利用したく追加したもの
  user = this.auth.user;
  // PWA及び通知機能を利用したく追加したもの
  message;

  constructor(
    // Authを利用したく追加したもの
    private auth: AuthService,
    // PWA及び通知機能を利用したく追加したもの
    private msgService: MessagingService
  ) {}

  ngOnInit() {
    // 認証状態の変更を監視しておく
    this.user = this.auth.angularFireAuth.authState;
    this.user.subscribe((u) => console.log(u.providerData));
    // PWA及び通知機能を利用したく追加したもの
    this.msgService.getPermission();
    this.msgService.reseiveMessage();
    this.message = this.msgService.currentMessage;
  }

  // ログアウト
  logout() {
    this.auth.authLogout();
  }
}
