import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfTypeComponent } from './list-of-type.component';

describe('ListOfTypeComponent', () => {
  let component: ListOfTypeComponent;
  let fixture: ComponentFixture<ListOfTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
