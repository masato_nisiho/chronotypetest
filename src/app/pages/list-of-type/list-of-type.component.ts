import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";

@Component({
  selector: "app-list-of-type",
  templateUrl: "./list-of-type.component.html",
  styleUrls: ["./list-of-type.component.css"],
})
export class ListOfTypeComponent implements OnInit {
  constructor(private location: Location) {}

  ngOnInit() {}

  // 1つ前に戻る
  goBack = (): void => this.location.back();
}
