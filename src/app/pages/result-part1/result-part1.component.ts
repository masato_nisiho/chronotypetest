import { Component, OnInit } from "@angular/core";
//ngrx
import { Store, select } from "@ngrx/store";
import { Observable, of } from "rxjs";

import { Location } from "@angular/common";

@Component({
  selector: "app-result-part1",
  templateUrl: "./result-part1.component.html",
  styleUrls: ["./result-part1.component.css"],
})
export class ResultPart1Component implements OnInit {
  // ngrx
  part1TotalScore$: Observable<number>;
  seven$: Observable<number>;

  constructor(
    private store: Store<{ count: number }>,
    private location: Location
  ) {
    this.part1TotalScore$ = store.pipe(select("count"));
    this.part1TotalScore$ = store.pipe(select("count"));
    this.seven$ = of(7);
  }

  ngOnInit() {}

  // 1つ前に戻る
  goBack(): void {
    this.location.back();
  }
}
