import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPart1Component } from './result-part1.component';

describe('ResultPart1Component', () => {
  let component: ResultPart1Component;
  let fixture: ComponentFixture<ResultPart1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultPart1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPart1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
