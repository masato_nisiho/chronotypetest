import { Component, OnInit } from "@angular/core";
import { Part2Question } from "../../interfaces/part2-question";
import { PART2QUESTIONS } from "../../mock-questions/mock-part2-questions";

import { Location } from "@angular/common";

//ngrx
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { assign } from "../../ngrx/part2-score.actions";

@Component({
  selector: "app-test-part2",
  templateUrl: "./test-part2.component.html",
  styleUrls: ["./test-part2.component.css"],
})
export class TestPart2Component implements OnInit {
  selectedPart2question: Part2Question;
  part2questions = PART2QUESTIONS;

  // ngrx
  part2TotalScore$: Observable<number>;

  constructor(
    private location: Location,
    private store: Store<{ count: number }>
  ) {
    this.part2TotalScore$ = store.pipe(select("count"));
  }

  ngOnInit() {}

  onSelectChoice(score: number, part2question: Part2Question): void {
    if (score === 1) {
      if (!part2question.choices[0].checked) {
        part2question.choices[0].checked = true;
        part2question.choices[1].checked = false;
        part2question.choices[2].checked = false;
      }
    } else if (score === 2) {
      if (!part2question.choices[1].checked) {
        part2question.choices[0].checked = false;
        part2question.choices[1].checked = true;
        part2question.choices[2].checked = false;
      }
    } else if (score === 3) {
      if (!part2question.choices[2].checked) {
        part2question.choices[0].checked = false;
        part2question.choices[1].checked = false;
        part2question.choices[2].checked = true;
      }
    } else {
      console.log("wrong!");
    }
  }

  // ✔した項目を数える。
  countChoices(): number {
    var total_choice: number[] = [0, 0, 0];

    for (let i = 0; i < this.part2questions.length; i++) {
      for (let j = 0; j < total_choice.length; j++) {
        if (this.part2questions[i].choices[j].checked) {
          total_choice[j]++;
        }
      }
    }

    return total_choice[0] + total_choice[1] + total_choice[2];
  }

  // ✔した点数を数える。
  countTotalScore(): number {
    var total_choice: number[] = [0, 0, 0];

    for (let i = 0; i < this.part2questions.length; i++) {
      for (let j = 0; j < total_choice.length; j++) {
        if (this.part2questions[i].choices[j].checked) {
          total_choice[j]++;
        }
      }
    }
    return total_choice[0] + total_choice[1] * 2 + total_choice[2] * 3;
  }

  // 1つ前に戻る
  goBack(): void {
    this.location.back();
  }

  assign_countTotalScore_To_part2TotalScore(countTotalScore: number): void {
    // ngrx
    this.store.dispatch(assign({ totalScore: countTotalScore }));
  }
}
