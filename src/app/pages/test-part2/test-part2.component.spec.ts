import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestPart2Component } from './test-part2.component';

describe('TestPart2Component', () => {
  let component: TestPart2Component;
  let fixture: ComponentFixture<TestPart2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestPart2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPart2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
