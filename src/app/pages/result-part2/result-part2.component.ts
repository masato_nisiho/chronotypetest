import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
//ngrx
import { Store, select } from "@ngrx/store";
import { Observable, of } from "rxjs";

@Component({
  selector: "app-result-part2",
  templateUrl: "./result-part2.component.html",
  styleUrls: ["./result-part2.component.css"],
})
export class ResultPart2Component implements OnInit {
  // ngrx
  part2TotalScore$: Observable<number>;
  totalscore_32$: Observable<number>;
  totalscore_48$: Observable<number>;

  constructor(
    private store: Store<{ count: number }>,
    private location: Location
  ) {
    this.part2TotalScore$ = store.pipe(select("count"));
    this.totalscore_32$ = of(32);
    this.totalscore_48$ = of(48);
  }

  ngOnInit(): void {}

  goBack = (): void => this.location.back();
}
