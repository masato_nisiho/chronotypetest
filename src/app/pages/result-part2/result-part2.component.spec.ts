import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultPart2Component } from './result-part2.component';

describe('ResultPart2Component', () => {
  let component: ResultPart2Component;
  let fixture: ComponentFixture<ResultPart2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultPart2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultPart2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
