import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestPart1Component } from './test-part1.component';

describe('TestPart1Component', () => {
  let component: TestPart1Component;
  let fixture: ComponentFixture<TestPart1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestPart1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestPart1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
