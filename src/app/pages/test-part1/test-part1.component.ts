import { Component, OnInit } from "@angular/core";
import { Part1Question } from "../../interfaces/part1-question";
import { PART1QUESTIONS } from "../../mock-questions/mock-part1-questions";

//ngrx
import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";
import { assign } from "../../ngrx/part1-score.actions";

@Component({
  selector: "app-test-part1",
  templateUrl: "./test-part1.component.html",
  styleUrls: ["./test-part1.component.css"],
})
export class TestPart1Component implements OnInit {
  selectedPart1question: Part1Question;
  part1questions = PART1QUESTIONS;

  // ngrx
  part1TotalScore$: Observable<number>;

  constructor(private store: Store<{ count: number }>) {
    this.part1TotalScore$ = store.pipe(select("count"));
  }

  ngOnInit() {}

  checkSelectedLength(length: number): void {
    console.log(length);
  }

  assign_SelectedLength_To_part1TotalScore(length: number): void {
    // ngrx
    this.store.dispatch(assign({ totalScore: length }));
  }
}
