import { Component, OnInit } from "@angular/core";

// Authを利用したく追加したもの
import { AuthService } from "../../service/auth/auth.service";

// DLマテリアルデザイン
import { DomSanitizer } from "@angular/platform-browser";
import { MatIconRegistry } from "@angular/material/icon";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  // Authを利用したく追加したもの
  user = this.auth.user;
  email: string;
  password: string;

  constructor(
    // Authを利用したく追加したもの
    private auth: AuthService,

    // DLマテリアルデザイン
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon(
      "twitter",
      sanitizer.bypassSecurityTrustResourceUrl("assets/icons/twitter.svg")
    );
    iconRegistry.addSvgIcon(
      "google",
      sanitizer.bypassSecurityTrustResourceUrl("assets/icons/google.svg")
    );
    iconRegistry.addSvgIcon(
      "anonymous3",
      sanitizer.bypassSecurityTrustResourceUrl("assets/icons/anonymous3.svg")
    );
  }

  ngOnInit() {
    // 認証状態の変更を監視しておく
    this.user = this.auth.angularFireAuth.authState;
    this.user.subscribe((u) => console.log(u.providerData));
  }

  // ログアウト
  logout() {
    this.auth.authLogout();
  }

  // Google認証によるログイン
  loginWithGoogle() {
    this.auth.authLoginWithGoogle();
  }

  // 匿名認証
  loginAnonymously() {
    this.auth.authLoginAnonymously();
  }

  // email,passwordによるログイン
  // async loginWithEmailAndPassword() {
  //   try {
  //     // ユーザの登録
  //     const credential = await this.angularFireAuth.auth.createUserWithEmailAndPassword(
  //       this.email,
  //       this.password
  //     );
  //   } catch (error) {
  //     if ((error.code = "auth/email-already-in-use")) {
  //       // すでに使われている場合、ログインに切り替える
  //       await this.angularFireAuth.auth.signInWithEmailAndPassword(
  //         this.email,
  //         this.password
  //       );
  //     }
  //     console.log(error);
  //   } finally {
  //     this.email = "";
  //     this.password = "";
  //   }
  loginWithEmailAndPassword() {
    this.auth.authLoginWithEmailAndPassword(this.email, this.password);
  }

  // twitter認証によるログイン
  loginWithTwitter() {
    this.auth.authLoginWithTwitter();
  }
}
