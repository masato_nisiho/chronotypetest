import { createReducer, on } from "@ngrx/store";
import { assign } from "./part2-score.actions";

export const initialState = 0;

const _counterReducer = createReducer(
  initialState,
  on(assign, (state, { totalScore }) => totalScore)
);

export function counterReducer(state, action) {
  return _counterReducer(state, action);
}
