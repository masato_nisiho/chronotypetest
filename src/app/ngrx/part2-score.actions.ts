import { createAction, props } from "@ngrx/store";

export const assign = createAction(
  "[Counter Component] Assign",
  props<{ totalScore: number }>()
);
