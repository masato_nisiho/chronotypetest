import { Part2Question } from "../interfaces/part2-question";

export const PART2QUESTIONS: Part2Question[] = [
  {
    id: 1,
    text:
      "次の日になにも予定がなくて何時に起きてもかまわないとしたら、あなたは何時に起きますか？",
    choices: [
      { choice_text: "6：30より前", checked: false },
      { choice_text: "6：30〜8：45のあいだ", checked: false },
      { choice_text: "8：45より後", checked: false },
    ],
  },
  {
    id: 2,
    text:
      "特定の時間に起きなければならないとしたら、あなたは目覚まし時計を使いますか？",
    choices: [
      {
        choice_text:
          "目覚まし時計は使わず、起きたいと思った時間に起きることができる",
        checked: false,
      },
      {
        choice_text: "目覚まし時計は使うが、スヌーズ機能は必要ない",
        checked: false,
      },
      {
        choice_text:
          "目覚まし時計を使うし、スヌーズ機能は必須。できればアラームを2〜3個はセットしたい",
        checked: false,
      },
    ],
  },
  {
    id: 3,
    text: "週末は何時に起きますか？",
    choices: [
      { choice_text: "平日と同じ", checked: false },
      { choice_text: "平日より45〜89分ほど遅い", checked: false },
      { choice_text: "平日より90分以上遅い", checked: false },
    ],
  },
  {
    id: 4,
    text: "あなたにとって時差ボケはどのような感覚ですか？",
    choices: [
      {
        choice_text: "いつまでたっても時差ボケが治らない",
        checked: false,
      },
      { choice_text: "48時間もあれば改善する", checked: false },
      { choice_text: "たいていすぐに改善する", checked: false },
    ],
  },
  {
    id: 5,
    text: "1日で一番好きな食事の時間は？",
    choices: [
      { choice_text: "朝食", checked: false },
      { choice_text: "昼食", checked: false },
      { choice_text: "夕食", checked: false },
    ],
  },
  {
    id: 6,
    text:
      "もし学生時代にもどってセンター試験を受けねばならないとしたら、テストの開始時間はいつがいいですか？",
    choices: [
      { choice_text: "朝", checked: false },
      { choice_text: "昼すぎ", checked: false },
      { choice_text: "15〜16ごろ", checked: false },
    ],
  },
  {
    id: 7,
    text: "激しい運動をしなければならないとしたら、何時ごろを選びますか？",
    choices: [
      { choice_text: "8：00前", checked: false },
      { choice_text: "8：00〜16：00のあいだ", checked: false },
      { choice_text: "16：00よりもあと", checked: false },
    ],
  },
  {
    id: 8,
    text: "1日のうちもっとも集中できるのはいつですか？",
    choices: [
      { choice_text: "起きてから1〜2時間後", checked: false },
      { choice_text: "起きてから2〜4時間後", checked: false },
      { choice_text: "起きてから4〜6時間後", checked: false },
    ],
  },
  {
    id: 9,
    text: "1日の労働時間が5時間だけだとしたら、どの時間帯に働きたいですか？",
    choices: [
      { choice_text: "4：00〜9：00", checked: false },
      { choice_text: "9：00〜14：00", checked: false },
      { choice_text: "16：00〜21：00", checked: false },
    ],
  },
  {
    id: 10,
    text: "自分の思考タイプは、以下のどれに当てはまると思いますか？",
    choices: [
      { choice_text: "分析と論理的な思考がうまい", checked: false },
      { choice_text: "論理と直感をあわせ持つバランス型", checked: false },
      { choice_text: "直感と創造的な思考がうまい", checked: false },
    ],
  },
  {
    id: 11,
    text: "昼寝をしますか？するとどうなりますか？",
    choices: [
      { choice_text: "まったくしない", checked: false },
      { choice_text: "仕事がないときにときどきする", checked: false },
      { choice_text: "昼寝をすると夜まで元気でいられる", checked: false },
    ],
  },
  {
    id: 12,
    text:
      "2時間の激しい肉体労働をしなければならないとしたら（重い家具の移動など）、もっとも効率的に動ける時間帯はいつだと思いますか？",
    choices: [
      { choice_text: "8：00〜10：00", checked: false },
      { choice_text: "11：00〜13：00", checked: false },
      { choice_text: "18：00〜20：00", checked: false },
    ],
  },
  {
    id: 13,
    text: "あなたの健康意識は、以下のどれに当てはまると思いますか？",
    choices: [
      {
        choice_text: "いつも健康的な食事や運動を実践している",
        checked: false,
      },
      {
        choice_text: "ときどき健康的な食事や運動を実践している",
        checked: false,
      },
      {
        choice_text: "健康的な食事や運動を実践するのは難しい",
        checked: false,
      },
    ],
  },
  {
    id: 14,
    text: "リスクを取ることに対して、どれぐらい不安になりますか？",
    choices: [
      { choice_text: "不安はあまりない", checked: false },
      { choice_text: "まぁまぁ不安がある", checked: false },
      { choice_text: "かなりの不安がある", checked: false },
    ],
  },
  {
    id: 15,
    text: "あなたは以下のどのタイプですか？",
    choices: [
      {
        choice_text: "未来志向で、明確な目標と大きなプランを持っている",
        checked: false,
      },
      {
        choice_text:
          "過去に学び、未来に希望を持ち、現在を生きる情熱を大事にしている",
        checked: false,
      },
      {
        choice_text: "つねに現在志向で、いまの幸福を大事にしている",
        checked: false,
      },
    ],
  },
  {
    id: 16,
    text: "学生時代、あなたはどんなタイプでしたか？",
    choices: [
      { choice_text: "人気者", checked: false },
      { choice_text: "真面目", checked: false },
      { choice_text: "怠け者", checked: false },
    ],
  },
  {
    id: 17,
    text: "朝起きたらどのような状態ですか？",
    choices: [
      { choice_text: "起きてすぐシャキシャキ行動できる", checked: false },
      {
        choice_text: "ボーっとするが寝床から出られないほどではない",
        checked: false,
      },
      {
        choice_text: "完全にフラフラで目を開くのも一苦労",
        checked: false,
      },
    ],
  },
  {
    id: 18,
    text: "起きて30分後の食欲はどのような状態ですか？",
    choices: [
      { choice_text: "かなりの空腹", checked: false },
      { choice_text: "やや空腹", checked: false },
      { choice_text: "まったく腹が減らない", checked: false },
    ],
  },
  {
    id: 19,
    text: "夜眠れないことがどれぐらいありますか？",
    choices: [
      {
        choice_text: "ほとんどない。海外旅行で時差ボケしたときぐらい",
        checked: false,
      },
      {
        choice_text: "あまりないが、ストレスが強いときには眠れなくなる",
        checked: false,
      },
      { choice_text: "慢性的に不眠", checked: false },
    ],
  },
  {
    id: 20,
    text: "人生の満足度はどれぐらいですか？",
    choices: [
      { choice_text: "高い", checked: false },
      { choice_text: "普通に良い", checked: false },
      { choice_text: "低い", checked: false },
    ],
  },
];
