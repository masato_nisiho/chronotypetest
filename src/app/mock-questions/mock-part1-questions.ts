import { Part1Question } from "../interfaces/part1-question";

export const PART1QUESTIONS: Part1Question[] = [
  {
    id: 1,
    text: "ちょっとした音や光でも目が覚めて眠れなくなる",
    answer: false,
  },
  { id: 2, text: "食事にあまり興味がない", answer: false },
  { id: 3, text: "だいたい目覚まし時計が鳴る前に起きる", answer: false },
  {
    id: 4,
    text: "飛行機に乗ると、アイマスクや耳栓をしても眠れない",
    answer: false,
  },
  { id: 5, text: "ときどき疲労感でイライラしてしまう", answer: false },
  { id: 6, text: "小さなことでやたらと不安になってしまう", answer: false },
  {
    id: 7,
    text:
      "正式に不眠症だと診断を受けたことがある。または、自己判断で不眠症だと感じる",
    answer: false,
  },
  {
    id: 8,
    text: "学生だったころは、いつも成績のことが不安だった",
    answer: false,
  },
  {
    id: 9,
    text: "つい過去や未来のことを考えてしまい、眠れなくなることがよくある",
    answer: false,
  },
  { id: 10, text: "自分を完璧主義者だと思う", answer: false },
];
