import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";

// Components
import { AppComponent } from "./app.component";
import { LoginComponent } from "./pages/login/login.component";
import { TestPart1Component } from "./pages/test-part1/test-part1.component";
import { TestPart2Component } from "./pages/test-part2/test-part2.component";
import { ResultPart1Component } from "./pages/result-part1/result-part1.component";
import { ResultPart2Component } from "./pages/result-part2/result-part2.component";
import { ListOfTypeComponent } from "./pages/list-of-type/list-of-type.component";

// ngrx
import { StoreModule } from "@ngrx/store";
import { counterReducer } from "./ngrx/part1-score.reducer";

// ???
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Angular Material
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatListModule } from "@angular/material/list";

// firebase Auth 利用時に追加したもの
import { FormsModule } from "@angular/forms";
import { environment } from "../environments/environment";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";

// PWA及び通知機能を利用したく追加したもの
import { ServiceWorkerModule } from "@angular/service-worker";
import { MessagingService } from "./service/messaging.service";
// 通知バグFIX
import { AngularFireDatabaseModule } from "angularfire2/database";
import { HttpClientModule } from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TestPart1Component,
    TestPart2Component,
    ResultPart1Component,
    ResultPart2Component,
    ListOfTypeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({ count: counterReducer }),
    BrowserAnimationsModule,

    // Angular Material
    MatButtonModule,
    MatIconModule,
    MatCheckboxModule,
    MatListModule,

    // firebase Auth 利用時に追加したもの
    FormsModule, // バインド用
    AngularFireModule.initializeApp(environment.firebase), // angularfireの設定
    AngularFireAuthModule, // angularfireのAuth用モジュール

    // PWA及び通知機能を利用したく追加したもの
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production,
    }),
    // 通知バグFIX
    AngularFireDatabaseModule,
    HttpClientModule,
  ],

  providers: [MessagingService],
  bootstrap: [AppComponent],
})
export class AppModule {}
