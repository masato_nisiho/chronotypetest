import { Injectable } from "@angular/core";
//  サービス化したい。appcomponent上で追加したくない
import * as firebase from "firebase/app";
import { Observable } from "rxjs";
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  //  サービス化したい。appcomponent上で追加したくない
  user: Observable<firebase.User>;
  email: string;
  password: string;

  constructor(public angularFireAuth: AngularFireAuth) {}

  // ログアウト
  async authLogout() {
    this.angularFireAuth.auth.signOut();
  }

  // Google認証によるログイン
  async authLoginWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    const credential = await this.angularFireAuth.auth.signInWithPopup(
      provider
    );
  }

  // 匿名認証
  async authLoginAnonymously() {
    const credential = await this.angularFireAuth.auth.signInAnonymously();
  }

  // email,passwordによるログイン
  async authLoginWithEmailAndPassword(e: string, p: string) {
    this.email = e;
    this.password = p;
    try {
      // ユーザの登録
      const credential = await this.angularFireAuth.auth.createUserWithEmailAndPassword(
        this.email,
        this.password
      );
    } catch (error) {
      if ((error.code = "auth/email-already-in-use")) {
        // すでに使われている場合、ログインに切り替える
        await this.angularFireAuth.auth.signInWithEmailAndPassword(
          this.email,
          this.password
        );
      }
      console.log(error);
    } finally {
      this.email = "";
      this.password = "";
    }
  }

  // twitter認証によるログイン
  async authLoginWithTwitter() {
    const provider = new firebase.auth.TwitterAuthProvider();
    const credential = await this.angularFireAuth.auth.signInWithPopup(
      provider
    );
  }
}
