import { Injectable } from "@angular/core";
import { AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { BehaviorSubject } from "rxjs";
import * as firebase from "firebase";
import "firebase/messaging";

@Injectable({
  providedIn: "root",
})
export class MessagingService {
  messaging = null;
  // safariでも表示されるようにここではNULLを代入
  // messaging = firebase.messaging();
  currentMessage = new BehaviorSubject(null);

  constructor(
    private db: AngularFireDatabase,
    private afAuth: AngularFireAuth
  ) {
    // safariでも表示されるようにif(firebase.messaging.isSupported())を利用
    if (firebase.messaging.isSupported()) {
      console.log("messagingはサポートされている。");

      this.messaging = firebase.messaging();
      // Add the public key generated from the console here.
      this.messaging.usePublicVapidKey(
        "BJo_BDlhlS6rkOnwWh8iDT7md1YPNIrBnM8K4k7tPNJ0BbVN39dW1LA7-sy3D8YLC1k3F2nAWwULOp-WKgE9ltU"
      );
    }
    // this.messaging.usePublicVapidKey(
    //   "BJo_BDlhlS6rkOnwWh8iDT7md1YPNIrBnM8K4k7tPNJ0BbVN39dW1LA7-sy3D8YLC1k3F2nAWwULOp-WKgE9ltU"
    // );
  }

  updateToken(token) {
    this.afAuth.authState.subscribe((user) => {
      if (!user) {
        return;
      }

      const data = { [user.uid]: token };
      this.db.object("fcmTokens/").update(data);
    });
  }

  getPermission() {
    this.messaging
      .requestPermission()
      .then(() => {
        console.log("Notification permission granted.");
        return this.messaging.getToken();
      })
      .then((token) => {
        console.log(token);
        this.updateToken(token);
      })
      .catch((err) => {
        console.log("Unable to get permission to notify.", err);
      });
  }

  reseiveMessage() {
    this.messaging.onMessage((payload) => {
      console.log("Message received. ", payload);
      this.currentMessage.next(payload);
    });
  }
}
