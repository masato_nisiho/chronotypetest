import * as functions from "firebase-functions";

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

import * as admin from "firebase-admin";
admin.initializeApp();

export const fcmSend = functions.database
  .ref("")
  .onCreate((snapshot, context) => {
    const articleInfo = snapshot.val();

    const payload = {
      notification: {
        title: "", // Pushメッセージのタイトル
        body: articleInfo.title + "が登録されました👏", // Pushメッセージ本文
        clickAction: "https://chronotypetest.firebaseapp.com", // Push通知をタップした時に、飛ばすURLを指定
        icon: "", // Push通知で使うロゴ
      },
    };

    admin
      .database()
      .ref("/fcmTokens/")
      .once("value")
      .then((token) => {
        const tokenList = token.val() || "";

        Object.keys(tokenList).forEach(function (key, index) {
          console.log(tokenList[key]);
          admin
            .messaging()
            .sendToDevice(tokenList[key], payload)
            .then((res) => {
              console.log("Sent Successfully", res);
            })
            .catch((err) => {
              console.log(err);
            });
        });
      })
      .catch((err) => {
        console.log(err);
      });
  });
